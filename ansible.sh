#!/bin/bash
#Ansible configuration 
amazon-linux-extras install epel -y

yum install ansible -y 

ansible --version

#Enable ansible logging by adding below line to 
echo "export log_path= ansible.log" >> /etc/ansible/ansible.cfg 

# Also add below line to disable host key verification [defaults]
#log_path = ./ansible.log
#host_key_checking = False
